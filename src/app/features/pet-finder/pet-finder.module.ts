import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetFinderRoutingModule } from './pet-finder-routing.module';
import { PetFinderComponent } from './pet-finder.component';
import { CardComponent } from './card/card.component';


@NgModule({
  declarations: [
    PetFinderComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    PetFinderRoutingModule
  ]
})
export class PetFinderModule { }
