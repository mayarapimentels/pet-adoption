import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PetFinderComponent } from './pet-finder.component';

const routes: Routes = [
  {
    path: "",
    component: PetFinderComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PetFinderRoutingModule { }
